/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* popup-base.c */
/**
 * SECTION:liblightwood-popupbase
 * @Title:LightwoodPopup
 * @Short_Description: Abstract class for popup widget.
 * @See_Also: #ClutterActor
 *
 * #LightwoodPopup will provide all generic properties required to create popup.In liblightwood variant 
 * popup style and theme can be different hence variant which needs popup widget will inherit
 * from LightwoodPopup. A generic properties like :timeout,and :sound-data and signals like popup 
 * shown and hidden properties are exposed.
 *
 * ## Sample C Code
 * |[<!-- language="C" -->
 * G_DEFINE_TYPE (LightwoodSelectionPopup, lightwood_selection_popup, POPUP_TYPE_BASE)
 *
 *#define LIGHTWOOD_SELECTION_POPUP_GET_PRIVATE(o) \
 *               (G_TYPE_INSTANCE_GET_PRIVATE ((o), LIGHTWOOD_TYPE_SELECTION_POPUP, LightwoodSelectionPopupPrivate))
 * // code goes here 
 *]| 
 */

#include "liblightwood-popupbase.h"


typedef enum _enPopupProperty enPopupProperty;
typedef enum _enPopupSignals enPopupSignals;

typedef struct
{
    gdouble timeout;
    gchar* sounddata;
} LightwoodPopupBasePrivate;

enum _enPopupProperty
{
    PROP_POPUP_FIRST,
    PROP_POPUP_TIMEOUT,
    PROP_POPUP_SOUND_DATA

};

enum _enPopupSignals
{
    SIG_POPUP_FIRST_SIGNAL,
    SIG_POPUP_SHOWN,                       /* Emitted when popup is shown */
    SIG_POPUP_HIDDEN,            		/* Emitted when popup is hidden */
    SIG_POPUP_ACTION,           	/* Emitted when action  is performed on popup (ex:popup-button press)*/
    SIG_POPUP_LAST_SIGNAL
};


guint32 popup_signals[SIG_POPUP_LAST_SIGNAL] = {0,};

G_DEFINE_ABSTRACT_TYPE_WITH_CODE (LightwoodPopupBase, lightwood_popup_base, CLUTTER_TYPE_ACTOR,
                                  G_ADD_PRIVATE (LightwoodPopupBase))

#define BASE_PRIVATE(o) lightwood_popup_base_get_instance_private ((LightwoodPopupBase *) o)

static void
lightwood_popup_base_get_property (GObject    *object,
                         guint       property_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
    LightwoodPopupBase *self = LIGHTWOOD_POPUP_BASE (object);

    switch (property_id)
    {
    case PROP_POPUP_TIMEOUT:
        g_value_set_double (value, lightwood_popup_base_get_timeout (self));
        break;
    case PROP_POPUP_SOUND_DATA:
        g_value_set_string (value, lightwood_popup_base_get_sound_data (self));
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
lightwood_popup_base_set_property (GObject      *object,
                         guint         property_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
    LightwoodPopupBase *self = LIGHTWOOD_POPUP_BASE (object);

    switch (property_id)
    {
    case PROP_POPUP_TIMEOUT:
        lightwood_popup_base_set_timeout (self, g_value_get_double (value));
        break;
    case PROP_POPUP_SOUND_DATA:
        lightwood_popup_base_set_sound_data (self, g_value_get_string (value));
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
lightwood_popup_base_dispose (GObject *object)
{
    G_OBJECT_CLASS (lightwood_popup_base_parent_class)->dispose (object);
}

static void
lightwood_popup_base_finalize (GObject *object)
{
  LightwoodPopupBasePrivate *priv = BASE_PRIVATE (object);

  g_free (priv->sounddata);

  G_OBJECT_CLASS (lightwood_popup_base_parent_class)->finalize (object);
}



static void
lightwood_popup_base_class_init (LightwoodPopupBaseClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    GParamSpec *pspec = NULL;

    object_class->get_property = lightwood_popup_base_get_property;
    object_class->set_property = lightwood_popup_base_set_property;
    object_class->dispose = lightwood_popup_base_dispose;
    object_class->finalize = lightwood_popup_base_finalize;

    /**
             * LightwoodPopupBase:timeout:
             *
             * timeout for the popup widget
             * DEFAULT:4
             */
    pspec = g_param_spec_double("timeout",
                                "TimeOut",
                                "time in seconds for popup to be shown",
                                0.0,
                                100.0,
                                4.0,
                                G_PARAM_READWRITE);
    g_object_class_install_property (object_class, PROP_POPUP_TIMEOUT, pspec);

    /**
                 * LightwoodPopupBase:sound-data:
                 *
                 * path of the media file which is to be played when popup is shown
                 */
    pspec = g_param_spec_string("sound-data",
                                "Sounddata",
                                "path of the media file which is to be played when popup is raised or shown",
                                (const gchar*)"path of media file",
                                G_PARAM_READWRITE);
    g_object_class_install_property (object_class, PROP_POPUP_SOUND_DATA, pspec);

    // Create a signal for the popup when popup is shown
            /**
             * LightwoodPopupBase::popup-shown:
             * @popupbase: The object which received the signal
             *
             * ::popup-shown is emitted when ever popup is shown
             */
    popup_signals[SIG_POPUP_SHOWN]=g_signal_new("popup-shown",
                                   G_TYPE_FROM_CLASS (object_class),
                                   G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
                                   G_STRUCT_OFFSET (LightwoodPopupBaseClass, popup_shown),
                                   NULL, NULL,
                                   g_cclosure_marshal_generic,
                                   G_TYPE_NONE, 0);
    // Create a signal for the popup when popup is hidden
               /**
                * LightwoodPopupBase::popup-hidden:
                * @popupbase: The object which received the signal
                *
                * ::popup-hidden is emitted when ever popup is hidden
                */
    popup_signals[SIG_POPUP_HIDDEN]=g_signal_new("popup-hidden",
                                    G_TYPE_FROM_CLASS (object_class),
                                    G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
                                    G_STRUCT_OFFSET (LightwoodPopupBaseClass, popup_hidden),
                                    NULL, NULL,
                                    g_cclosure_marshal_generic,
                                    G_TYPE_NONE, 0);

    // Create a signal for the popup when popup button is pressed
                   /**
                    * LightwoodPopupBase::popup-action:
                    * @popupbase: The object which received the signal
                    * @popup: the the name (as returned by clutter_actor_get_name()) of the popup
                    * @FIXME: what is this argument?!
                    *
                    * ::popup-action is emitted when ever button press and release action is performmed on popup
                    */
    popup_signals[SIG_POPUP_ACTION]=g_signal_new("popup-action",
                                    G_TYPE_FROM_CLASS (object_class),
                                    G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
                                    G_STRUCT_OFFSET (LightwoodPopupBaseClass, popup_action),
                                    NULL, NULL,
                                    g_cclosure_marshal_generic,
                                    G_TYPE_NONE, 2, G_TYPE_STRING,G_TYPE_POINTER);

}

static void
lightwood_popup_base_init (LightwoodPopupBase *self)
{
  LightwoodPopupBasePrivate *priv = BASE_PRIVATE (self);

  priv->timeout = 4.0;
  priv->sounddata = NULL;
}

/**
 * lightwood_popup_base_get_timeout:
 * @self: a #LightwoodPopupBase
 *
 * Return the #LightwoodPopupBase:timeout property
 *
 * Returns: the value of #LightwoodPopupBase:timeout property
 */
gdouble
lightwood_popup_base_get_timeout (LightwoodPopupBase *self)
{
  LightwoodPopupBasePrivate *priv = BASE_PRIVATE (self);

  g_return_val_if_fail (LIGHTWOOD_IS_POPUP_BASE (self), 0.0);

  return priv->timeout;
}

/**
 * lightwood_popup_base_set_timeout:
 * @self: a #LightwoodPopupBase
 * @timeout: the new timeout value
 *
 * Update the #LightwoodPopupBase:timeout property
 */
void
lightwood_popup_base_set_timeout (LightwoodPopupBase *self,
                                  gdouble timeout)
{
  LightwoodPopupBasePrivate *priv = BASE_PRIVATE (self);

  g_return_if_fail (LIGHTWOOD_IS_POPUP_BASE (self));

  priv->timeout = timeout;
  g_object_notify (G_OBJECT (self), "timeout");
}

/**
 * lightwood_popup_base_get_sound_data:
 * @self: a #LightwoodPopupBase
 *
 * Return the #LightwoodPopupBase:sound-data property
 *
 * Returns: the value of #LightwoodPopupBase:sound-data property
 */
const gchar *
lightwood_popup_base_get_sound_data (LightwoodPopupBase *self)
{
  LightwoodPopupBasePrivate *priv = BASE_PRIVATE (self);

  g_return_val_if_fail (LIGHTWOOD_IS_POPUP_BASE (self), NULL);

  return priv->sounddata;
}

/**
 * lightwood_popup_base_set_sound_data:
 * @self: a #LightwoodPopupBase
 * @sound: path to the new sound file
 *
 * Update the #LightwoodPopupBase:sound-data property
 */
void
lightwood_popup_base_set_sound_data (LightwoodPopupBase *self,
                                     const gchar *sound)
{
  LightwoodPopupBasePrivate *priv = BASE_PRIVATE (self);

  g_return_if_fail (LIGHTWOOD_IS_POPUP_BASE (self));
  g_return_if_fail (sound != NULL);

  g_free (priv->sounddata);
  priv->sounddata = g_strdup (sound);

  g_object_notify (G_OBJECT (self), "timeout");
}
