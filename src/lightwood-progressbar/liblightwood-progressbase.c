/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* progress_base.c */

/**
 * SECTION:liblightwood-progressbase
 * @Title:LightwoodProgressBar
 * @Short_Description: Abstract class for progress bar.
 * @See_Also: #LightwoodModel #ClutterActor
 *
 * #LightwoodProgressBar will provide all generic properties required to create a progress bar inheriting 
 * from #LightwoodProgressBar. This abstract provides seek end,seek start,update progress bar and top 
 * to seek,play started,pause featues hence making it intuitive.It can be inherited like this:
 * 
 * ## Sample C Code
 * |[<!-- language="C" -->
 * G_DEFINE_TYPE (LightwoodProgressBar, lightwood_progress_bar, PROGRESS_TYPE_BASE)
 * #define PROGRESS_BAR_PRIVATE(o) \
 *               (G_TYPE_INSTANCE_GET_PRIVATE ((o), LIGHTWOOD_TYPE_PROGRESS_BAR, LightwoodProgressBarPrivate))
 * ]|
 */

#include "liblightwood-progressbase.h"

typedef struct
{
	ClutterActor *playPauseGroup;
	ClutterAction *action;
	gdouble seekButtonSize;
	gboolean playState;
	gboolean playing;
	gdouble currentDuration;
	gdouble bufferFill;
	gdouble progressBarOffset;
} LightwoodProgressBasePrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (LightwoodProgressBase, lightwood_progress_base, CLUTTER_TYPE_ACTOR)

#define BASE_PRIVATE(o) lightwood_progress_base_get_instance_private ((LightwoodProgressBase *) o)

/*Properties for the progress bar.*/
enum ProgressBarProperty
{
	PROP_LIGHTWOOD_PROGRESS_BAR_FIRST,
	PROP_LIGHTWOOD_PROGRESS_BAR_PLAY_STATE,			/* Refers to the progress bar state for Play/Pause as TRUE/FALSE */
	PROP_LIGHTWOOD_PROGRESS_BAR_CURRENT_DURATION,	/* Refers to the progress bar current duration */
	PROP_LIGHTWOOD_PROGRESS_BAR_BUFFER_FILL,        /* Refers to the progress bar buffer-fill*/
	PROP_LIGHTWOOD_PROGRESS_BAR_OFFSET,				/* Refers to the progress bar offset */
	PROP_LIGHTWOOD_PROGRESS_BAR_SEEK_BUTTON_SIZE,
	PROP_LIGHTWOOD_PROGRESS_BAR_LAST
};

/* The signals emitted by the progress bar */
enum ProgressBarSignals
{
	SIG_LIGHTWOOD_PROGRESS_BAR_SEEK_END,			/* Emitted when user seeks on the progress bar */
	SIG_LIGHTWOOD_PROGRESS_BAR_PLAY_REQUESTED,		/* Emitted when play is requested by the user on progress bar */
	SIG_LIGHTWOOD_PROGRESS_BAR_PAUSE_REQUESTED,		/* Emitted when pause is requested by the user on the progressbar */
	SIG_LIGHTWOOD_PROGRESS_BAR_PROGRESS_UPDATED,
	SIG_LIGHTWOOD_PROGRESS_BAR_SEEK_START,
	SIG_LIGHTWOOD_PROGRESS_BAR_LAST_SIGNAL
};


/* Storage for the signals */
static guint32 lightwood_progress_bar_signals[SIG_LIGHTWOOD_PROGRESS_BAR_LAST_SIGNAL] = {0,};


/* static APIs for progress bar property set methods */
static void set_progress_bar_play_state(GObject *object, const GValue *value,GParamSpec *pspec);

static void lightwood_progress_bar_get_property (GObject *object, guint propertyId, GValue  *value, GParamSpec *pspec);
static void lightwood_progress_bar_set_property (GObject *object, guint propertyId, const GValue *value, GParamSpec *pspec);


static void lightwood_progress_bar_get_property (GObject *object, guint propertyId, GValue  *value, GParamSpec *pspec)
{
	LightwoodProgressBase *self = LIGHTWOOD_PROGRESS_BASE (object);

	/* Based on the property ID take the appropriate action (Call the
	  respective method to get the property value) */
	switch(propertyId)
	{
	case PROP_LIGHTWOOD_PROGRESS_BAR_PLAY_STATE:
		g_value_set_boolean (value, lightwood_progress_base_get_play_state (self));
		break;

	case PROP_LIGHTWOOD_PROGRESS_BAR_CURRENT_DURATION:
		g_value_set_double (value, lightwood_progress_base_get_current_duration (self));
		break;

	case PROP_LIGHTWOOD_PROGRESS_BAR_BUFFER_FILL:
		g_value_set_double (value, lightwood_progress_base_get_buffer_fill (self));
		break;

	case PROP_LIGHTWOOD_PROGRESS_BAR_OFFSET:
		g_value_set_double (value, lightwood_progress_base_get_offset (self));
		break;
	case PROP_LIGHTWOOD_PROGRESS_BAR_SEEK_BUTTON_SIZE:
		g_value_set_double (value, lightwood_progress_base_get_seek_button_size (self));
		break;
		/* In case, the property is not installed for this object,
		    throw an appropriate warning */
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, propertyId, pspec);
	}
}

/*********************************************************************************************
 * Function:    lightwood_progress_bar_set_property
 * Description: set a property value
 * Parameters:  The object reference, property Id, new value
 *              for the property and the param spec of the object
 * Return:      void
 **********************************************************************************************/
static void lightwood_progress_bar_set_property (GObject *object, guint propertyId, const GValue *value, GParamSpec *pspec)
{
	LightwoodProgressBase *self = LIGHTWOOD_PROGRESS_BASE (object);

	/* Based on the property ID take the appropriate action (Call the
	  respective method to set the property value) */
	switch(propertyId)
	{
	case PROP_LIGHTWOOD_PROGRESS_BAR_PLAY_STATE:
		set_progress_bar_play_state(object, value,pspec);
		break;

	case PROP_LIGHTWOOD_PROGRESS_BAR_CURRENT_DURATION:
		lightwood_progress_base_set_current_duration (self, g_value_get_double (value));
		break;

	case PROP_LIGHTWOOD_PROGRESS_BAR_BUFFER_FILL:
		lightwood_progress_base_set_buffer_fill (self, g_value_get_double (value));
		break;
	case PROP_LIGHTWOOD_PROGRESS_BAR_OFFSET:
		lightwood_progress_base_set_offset (self, g_value_get_double (value));
		break;
	case PROP_LIGHTWOOD_PROGRESS_BAR_SEEK_BUTTON_SIZE:
		lightwood_progress_base_set_seek_button_size (self, g_value_get_double (value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, propertyId, pspec);
	}
}

/**
 * lightwood_progress_base_get_seek_button_size:
 * @self: a #LightwoodProgressBase
 *
 * Return the #LightwoodProgressBase:seek-button-size property
 *
 * Returns: the value of #LightwoodProgressBase:seek-button-size property
 */
gdouble
lightwood_progress_base_get_seek_button_size (LightwoodProgressBase *self)
{
  LightwoodProgressBasePrivate *priv = BASE_PRIVATE (self);

  g_return_val_if_fail (LIGHTWOOD_IS_PROGRESS_BASE (self), 0.0);

  return priv->seekButtonSize;
}

/**
 * lightwood_progress_base_get_current_duration:
 * @self: a #LightwoodProgressBase
 *
 * Return the #LightwoodProgressBase:current-duration property
 *
 * Returns: the value of #LightwoodProgressBase:current-duration property
 */
gdouble
lightwood_progress_base_get_current_duration (LightwoodProgressBase *self)
{
  LightwoodProgressBasePrivate *priv = BASE_PRIVATE (self);

  g_return_val_if_fail (LIGHTWOOD_IS_PROGRESS_BASE (self), 0.0);

  return priv->currentDuration;
}

/**
 * lightwood_progress_base_get_offset:
 * @self: a #LightwoodProgressBase
 *
 * Return the #LightwoodProgressBase:offset property
 *
 * Returns: the value of #LightwoodProgressBase:offset property
 */
gdouble
lightwood_progress_base_get_offset (LightwoodProgressBase *self)
{
  LightwoodProgressBasePrivate *priv = BASE_PRIVATE (self);

  g_return_val_if_fail (LIGHTWOOD_IS_PROGRESS_BASE (self), 0.0);

  return priv->progressBarOffset;
}

/**
 * lightwood_progress_base_get_buffer_fill:
 * @self: a #LightwoodProgressBase
 *
 * Return the #LightwoodProgressBase:buffer-fill property
 *
 * Returns: the value of #LightwoodProgressBase:buffer-fill property
 */
gdouble
lightwood_progress_base_get_buffer_fill (LightwoodProgressBase *self)
{
  LightwoodProgressBasePrivate *priv = BASE_PRIVATE (self);

  g_return_val_if_fail (LIGHTWOOD_IS_PROGRESS_BASE (self), 0.0);

  return priv->bufferFill;
}

/**
 * lightwood_progress_base_set_seek_button_size:
 * @self: a #LightwoodProgressBase
 * @size: the new size value
 *
 * Update the #LightwoodProgressBase:seek-button-size property
 */
void
lightwood_progress_base_set_seek_button_size (LightwoodProgressBase *self,
                                              gdouble size)
{
	LightwoodProgressBasePrivate *priv = BASE_PRIVATE (self);

	g_return_if_fail (LIGHTWOOD_IS_PROGRESS_BASE (self));

	/* set the width of progress bar */
	if(priv->seekButtonSize == size)
		return;
	priv->seekButtonSize = size;
	g_object_notify (G_OBJECT (self), "seek-button-size");

}

/**
 * lightwood_progress_base_set_offset:
 * @self: a #LightwoodProgressBase
 * @offset: the new offset value
 *
 * Update the #LightwoodProgressBase:offset property
 */
void
lightwood_progress_base_set_offset (LightwoodProgressBase *self,
                                    gdouble offset)
{
	LightwoodProgressBasePrivate *priv = BASE_PRIVATE (self);

	g_return_if_fail (LIGHTWOOD_IS_PROGRESS_BASE (self));
	/* set the width of progress bar */

	if (priv->progressBarOffset == offset)
		return;
	priv->progressBarOffset = offset;
	g_object_notify (G_OBJECT (self), "offset");
}


/*********************************************************************************************
 * Function:    set_progress_bar_play_state
 * Description: Set the progress bar play state property
 * Parameters:  The object reference, and property value for the
 *              the property
 * Return:      void
 ********************************************************************************************/
static void set_progress_bar_play_state (GObject *object, const GValue *value, GParamSpec *pspec)
{
	LightwoodProgressBasePrivate *priv = BASE_PRIVATE (object);
	gboolean recievedState = g_value_get_boolean (value);

	if(!LIGHTWOOD_PROGRESS_BASE(object))
		return;

	if(priv->playState == recievedState)
		return;
	/* set the play state */
	priv->playState = recievedState;
	g_object_notify_by_pspec(object,pspec);
}

/*********************************************************************************************
 * Function:    set_progress_bar_reactive
 * Description: Set the progress bar play state property
 * Parameters:  The object reference, and property value for the
 *              the property
 * Return:      void
 ********************************************************************************************/
static void
reactive_notify_cb (GObject *actor,
                    GParamSpec *spec,
                    LightwoodProgressBase *self)
{
	LightwoodProgressBasePrivate *priv = BASE_PRIVATE (self);
	gboolean reactive = clutter_actor_get_reactive (CLUTTER_ACTOR (actor));

	if (reactive)
	{
		clutter_drag_action_set_drag_threshold (CLUTTER_DRAG_ACTION (priv->action),10,10);
		clutter_drag_action_set_drag_axis (CLUTTER_DRAG_ACTION (priv->action),CLUTTER_DRAG_X_AXIS);
		clutter_actor_add_action (priv->playPauseGroup, priv->action);
	}
	else
	{
		//clutter_actor_remove_action (priv->playPauseGroup, priv->action);
	}
}

/**
 * lightwood_progress_base_set_current_duration:
 * @self: a #LightwoodProgressBase
 * @duration: the new duration value
 *
 * Update the #LightwoodProgressBase:current-duration property
 */
void
lightwood_progress_base_set_current_duration (LightwoodProgressBase *self,
                                              gdouble duration)
{
	LightwoodProgressBasePrivate *priv = BASE_PRIVATE (self);
	ClutterActor *actor = CLUTTER_ACTOR (self);
	gfloat progressBarPos;

	g_return_if_fail (LIGHTWOOD_IS_PROGRESS_BASE (self));

	// Its assumed to be playing or start play

	duration = CLAMP (duration, 0.0, 1.0);
	//g_print("CurrentValue in setter %lf \n",duration);
	progressBarPos = duration * ((clutter_actor_get_width (actor) - priv->progressBarOffset) - (priv->seekButtonSize));
	if(priv->currentDuration == progressBarPos)
		return;
	priv->currentDuration = progressBarPos;
	g_object_notify (G_OBJECT (self), "current-duration");
	//update_progress_bar (LIGHTWOOD_PROGRESS_BASE(object), progressBarPos);
}

/**
 * lightwood_progress_base_set_buffer_fill:
 * @self: a #LightwoodProgressBase
 * @fill: the new duration buffer-fill value
 *
 * Update the #LightwoodProgressBase:buffer-fill property
 */
void
lightwood_progress_base_set_buffer_fill (LightwoodProgressBase *self,
                                         gdouble fill)
{
	LightwoodProgressBasePrivate *priv = BASE_PRIVATE (self);
	ClutterActor *actor = CLUTTER_ACTOR (self);
	gfloat bufferPos;

	g_return_if_fail (LIGHTWOOD_IS_PROGRESS_BASE (self));

	fill = CLAMP (fill, 0.0, 1.0);
	priv->bufferFill = fill;
	bufferPos = fill * (clutter_actor_get_width (actor) - priv->progressBarOffset) - (priv->seekButtonSize);
	//clutter_actor_set_x (priv->playPauseGroup, bufferPos);
	if(priv->bufferFill == bufferPos)
		return;
	priv->bufferFill = bufferPos;
	g_object_notify (G_OBJECT (self), "buffer-fill");
	//clutter_actor_set_clip(priv->bufferRect, 0, 0, bufferPos, clutter_actor_get_height(priv->bufferRect) );
}

static void
lightwood_progress_bar_dispose (GObject *object)
{
	G_OBJECT_CLASS (lightwood_progress_base_parent_class)->dispose (object);
}

static void
lightwood_progress_bar_finalize (GObject *object)
{
	G_OBJECT_CLASS (lightwood_progress_base_parent_class)->finalize (object);
}

static void
lightwood_progress_base_class_init (LightwoodProgressBaseClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GParamSpec *pspec = NULL;

	object_class->get_property = lightwood_progress_bar_get_property;
	object_class->set_property = lightwood_progress_bar_set_property;
	object_class->dispose = lightwood_progress_bar_dispose;
	object_class->finalize = lightwood_progress_bar_finalize;

	/* Install the play-state property */
	pspec = g_param_spec_boolean("play-state", "PLAY-STATE", "State to indicate if progress bar is playing or paused",
			FALSE, G_PARAM_READWRITE);
	g_object_class_install_property(object_class, PROP_LIGHTWOOD_PROGRESS_BAR_PLAY_STATE, pspec);

	/* Install the current-duration property */
	pspec = g_param_spec_double("current-duration", "CURRENT-DURATION","The progress bar current duration",
			-G_MAXDOUBLE, G_MAXDOUBLE,0.0, G_PARAM_READWRITE);
	g_object_class_install_property(object_class, PROP_LIGHTWOOD_PROGRESS_BAR_CURRENT_DURATION, pspec);

	/* Install the buffer-fill property */
	pspec = g_param_spec_double("buffer-fill", "BUFFER-FILL","currently buffered duration",
			-G_MAXDOUBLE, G_MAXDOUBLE,0.0, G_PARAM_READWRITE);
	g_object_class_install_property(object_class, PROP_LIGHTWOOD_PROGRESS_BAR_BUFFER_FILL, pspec);

	/* Install the progress-bar-offset property */
	pspec = g_param_spec_double("offset", "PROGRESS-BAR-OFFSET","offset for the slider",
			-G_MAXDOUBLE, G_MAXDOUBLE,0.0, G_PARAM_READWRITE);
	g_object_class_install_property(object_class, PROP_LIGHTWOOD_PROGRESS_BAR_OFFSET, pspec);

	pspec = g_param_spec_double("seek-button-size", "SEEK-BUTTON-SIZE", "The seek button size", 0.0,1000.0,
			0.0, G_PARAM_READWRITE);
	g_object_class_install_property(object_class, PROP_LIGHTWOOD_PROGRESS_BAR_SEEK_BUTTON_SIZE, pspec);

	/* Create the seek update signal and add it to the class */
	/**
         * LightwoodProgressBar::seek-end:
         *
         * #LightwoodProgressBar emits a signal when seeking ended. 
         * 
         */
	lightwood_progress_bar_signals[SIG_LIGHTWOOD_PROGRESS_BAR_SEEK_END] = g_signal_new("seek-end",
			G_TYPE_FROM_CLASS (object_class),
			G_SIGNAL_NO_RECURSE |
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (LightwoodProgressBaseClass, seek_end),
			NULL, NULL,
			g_cclosure_marshal_VOID__FLOAT,
			G_TYPE_NONE, 1,
			G_TYPE_FLOAT,
			NULL);
	/**
         * LightwoodProgressBar::seek-start:
         *
         * #LightwoodProgressBar emits a signal when seeking started. 
         * 
         */

	lightwood_progress_bar_signals[SIG_LIGHTWOOD_PROGRESS_BAR_SEEK_START] = g_signal_new("seek-start",
			G_TYPE_FROM_CLASS (object_class),
			G_SIGNAL_NO_RECURSE |
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (LightwoodProgressBaseClass, seek_start),
			NULL, NULL,
			g_cclosure_marshal_VOID__FLOAT,
			G_TYPE_NONE, 1,
			G_TYPE_FLOAT,
			NULL);
	/**
         * LightwoodProgressBar::progress-updated:
         *
         * #LightwoodProgressBar emits a signal when progress is updated
         * 
         */


	lightwood_progress_bar_signals[SIG_LIGHTWOOD_PROGRESS_BAR_PROGRESS_UPDATED] = g_signal_new("progress-updated",
			G_TYPE_FROM_CLASS (object_class),
			G_SIGNAL_NO_RECURSE |
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (LightwoodProgressBaseClass, progress_updated),
			NULL, NULL,
			g_cclosure_marshal_VOID__FLOAT,
			G_TYPE_NONE, 1,
			G_TYPE_FLOAT,
			NULL);
/**
         * LightwoodProgressBar::tap-to-seek:
         *
         * #LightwoodProgressBar emits a signal when tap to seek is successfull.
         * 
         */


	lightwood_progress_bar_signals[SIG_LIGHTWOOD_PROGRESS_BAR_SEEK_START] = g_signal_new("tap-to-seek",
                        G_TYPE_FROM_CLASS (object_class),
                        G_SIGNAL_NO_RECURSE |
                        G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (LightwoodProgressBaseClass, tap_to_seek),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__FLOAT,
                        G_TYPE_NONE, 1,
                        G_TYPE_FLOAT,
                        NULL);
	/**
         * LightwoodProgressBar::play-requested:
         *
         * #LightwoodProgressBar emits a signal when starts to play
         * 
         */


	/* Create the play requested signal and add it to the class */
	lightwood_progress_bar_signals[SIG_LIGHTWOOD_PROGRESS_BAR_PLAY_REQUESTED] = g_signal_new("play-requested",
			G_TYPE_FROM_CLASS (object_class),
			G_SIGNAL_NO_RECURSE |
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (LightwoodProgressBaseClass, play_requested),
			NULL, NULL,
			g_cclosure_marshal_VOID__VOID,
			G_TYPE_NONE, 0,
			NULL);
	/**
         * LightwoodProgressBar::pause-requested:
         *
         * #LightwoodProgressBar emits a signal when Paused
         * 
         */


	/* Create the pause requested signal and add it to the class */
	lightwood_progress_bar_signals[SIG_LIGHTWOOD_PROGRESS_BAR_PAUSE_REQUESTED] = g_signal_new("pause-requested",
			G_TYPE_FROM_CLASS (object_class),
			G_SIGNAL_NO_RECURSE |
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (LightwoodProgressBaseClass, pause_requested),
			NULL, NULL,
			g_cclosure_marshal_VOID__VOID,
			G_TYPE_NONE, 0,
			NULL);



}

 /**
 * lightwood_progress_base_get_play_pause_group:
 * @self: a #LightwoodProgressBase
 *
 * This Function will get the actor contains group of play ,pause actors.
 * This is provided because to set the reactive to the group in derived class.
 *
 * Returns: (transfer none): container.
 */
ClutterActor *
lightwood_progress_base_get_play_pause_group (LightwoodProgressBase *self)
{
	LightwoodProgressBasePrivate *priv = BASE_PRIVATE (self);

	return priv->playPauseGroup;
}
 /**
 * lightwood_progress_base_get_drag_action:
 * @self: a #LightwoodProgressBase
 *
 * This Function will get the drag action actor.
 *
 * Returns: (transfer none): Action actor.
 */
ClutterAction *
lightwood_progress_base_get_drag_action (LightwoodProgressBase *self)
{
	LightwoodProgressBasePrivate *priv = BASE_PRIVATE (self);
	ClutterActor *actor = CLUTTER_ACTOR (self);

#ifdef CLUTTER_AVAILABLE_IN_1_12
	ClutterRect rect;
	rect.origin.x =0.0;
	rect.origin.y = 0.0;
	rect.size.width = (clutter_actor_get_width (actor) - priv->progressBarOffset)-(priv->seekButtonSize) ;
	rect.size.height = 50;
	g_object_set(priv->action,"drag-area",&rect,NULL);
#endif
	return priv->action;
}
 /**
 * lightwood_set_play_state:
 * @self: a #LightwoodProgressBase
 * @state :%TRUE/%FALSE
 *
 * This Function will set the play state in base .
 *
 */
void
lightwood_progress_base_set_play_state (LightwoodProgressBase *self,
                                        gboolean state)
{
	LightwoodProgressBasePrivate *priv = BASE_PRIVATE (self);
	priv->playState=state;

}

#define DEFAULT_WIDTH 375.0

static void
lightwood_progress_base_init (LightwoodProgressBase *self)
{
	LightwoodProgressBasePrivate *priv = BASE_PRIVATE (self);

	priv->playState = TRUE;
	priv->seekButtonSize=0.0;
	priv->currentDuration = 0.0;
	priv->progressBarOffset=20.0;
	priv->playPauseGroup=clutter_actor_new();
	priv->action = clutter_drag_action_new ();

	clutter_actor_set_width (CLUTTER_ACTOR (self), DEFAULT_WIDTH);

	g_signal_connect (self, "notify::reactive",
	    G_CALLBACK (reactive_notify_cb), self);
}

/**
 * lightwood_progress_base_get_play_state:
 * @self: a #LightwoodProgressBase
 *
 * Return the #LightwoodProgressBase:play-state property
 *
 * Returns: the value of #LightwoodProgressBase:play-state property
 */
gboolean
lightwood_progress_base_get_play_state (LightwoodProgressBase *self)
{
  LightwoodProgressBasePrivate *priv = BASE_PRIVATE (self);

  g_return_val_if_fail (LIGHTWOOD_IS_PROGRESS_BASE (self), FALSE);

  return priv->playState;
}
