/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION:liblightwood-cylinderdeform
 * @Title:LightwoodCylinderDeformEffect
 * @short_description: Effect that projects an actor to a cylinder.
 * 
 * #LightwoodCylinderDeformEffect is used to apply the effect to #ClutterActor. These 
 * effects are created using GLSL. Ideally #LightwoodRoller is using this effect.
 *
 * ## Sample C Code
 * |[<!-- language="C" -->
 * // to add the effect
 * ClutterEffect *cylinder_effect = lightwood_cylinder_deform_effect_new ();
 *  clutter_actor_add_effect_with_name (CLUTTER_ACTOR (roller),
 *                                          CYLINDER_DISTORT_EFFECT_NAME,
 *                                         cylinder_effect);
 * // to remove the effect
 *   clutter_actor_remove_effect_by_name (CLUTTER_ACTOR (roller),
 *                                        CYLINDER_DISTORT_EFFECT_NAME);
 * 
 *]|
 */

#include "liblightwood-cylinderdeform.h"

#include <math.h>

G_DEFINE_TYPE (LightwoodCylinderDeformEffect, lightwood_cylinder_deform_effect, CLUTTER_TYPE_SHADER_EFFECT)

static gchar *shader_source =
"#define PI 3.141592653589793238462643383279\n"
"\n"
"uniform sampler2D tex;\n"
"\n"
"void main () {\n"
"    float radius, y;\n"
"\n"
"    radius = 1.0 / PI;\n"
"    y = radius * acos (1.0 - cogl_tex_coord_in[0].t / radius);\n"
"\n"
"    gl_FragColor = texture2D(tex, vec2(cogl_tex_coord_in[0].s, y));\n"
"}";

/* ClutterOffscreenEffect implementation */

static CoglHandle
offscreen_effect_create_texture (ClutterOffscreenEffect *effect,
                                 gfloat                  width,
                                 gfloat                  height)
{
  ClutterActor *actor = clutter_actor_meta_get_actor (CLUTTER_ACTOR_META (effect));
  ClutterActorBox box;
  gfloat radius;

  clutter_actor_get_allocation_box (actor, &box);

  radius = (box.y2 - box.y1) / 2;
  return cogl_texture_new_with_size (box.x2 - box.x1,
                                     M_PI * radius,
                                     COGL_TEXTURE_NO_SLICING,
                                     COGL_PIXEL_FORMAT_RGBA_8888_PRE);
}

static void
offscreen_effect_paint_target (ClutterOffscreenEffect *effect)
{
  ClutterShaderEffect *shader = CLUTTER_SHADER_EFFECT (effect);
  ClutterOffscreenEffectClass *parent_class;
  CoglMaterial *target;

  clutter_shader_effect_set_uniform (shader, "tex", G_TYPE_INT, 1, 0);

  parent_class = CLUTTER_OFFSCREEN_EFFECT_CLASS (lightwood_cylinder_deform_effect_parent_class);
  parent_class->paint_target (effect);

  target = clutter_offscreen_effect_get_target (effect);
  cogl_material_set_layer_filters (target, 0,
                                   COGL_MATERIAL_FILTER_LINEAR,
                                   COGL_MATERIAL_FILTER_LINEAR);
}

static gchar *
shader_effect_get_static_shader_source (ClutterShaderEffect *effect)
{
  return g_strdup (shader_source);
}

/* GObject implementation */

static void
lightwood_cylinder_deform_effect_class_init (LightwoodCylinderDeformEffectClass *klass)
{
  ClutterOffscreenEffectClass *offscreen_class = CLUTTER_OFFSCREEN_EFFECT_CLASS (klass);
  ClutterShaderEffectClass *shader_class = CLUTTER_SHADER_EFFECT_CLASS (klass);

  offscreen_class->paint_target = offscreen_effect_paint_target;
  offscreen_class->create_texture = offscreen_effect_create_texture;

  shader_class->get_static_shader_source = shader_effect_get_static_shader_source;
}

static void
lightwood_cylinder_deform_effect_init (LightwoodCylinderDeformEffect *self)
{
}
/**
 * lightwood_cylinder_deform_effect_new:
 *
 * This function will instantiates the object of type #LightwoodCylinderDeformEffect.
 * Note that, as a #ClutterEffect, the returned object has a floating reference.
 *
 * Returns: (transfer full): #ClutterEffect instance.
 */
ClutterEffect *
lightwood_cylinder_deform_effect_new (void)
{
  return g_object_new (LIGHTWOOD_TYPE_CYLINDER_DEFORM_EFFECT, NULL);
}
