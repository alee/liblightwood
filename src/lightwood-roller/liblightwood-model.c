/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION:Lightwood-model
 * @short_description: #ThornburyModel subclass that adds a property for
 * monitoring when it stalls waiting for data (generally from a remote source).
 *
 * Users of #ThornburyModel can display an indicator when the associated #ThornburyModel
 * is waiting for data.
 */

#include "liblightwood-model.h"

G_DEFINE_ABSTRACT_TYPE (LightwoodModel, lightwood_model, THORNBURY_TYPE_MODEL)

enum
{
  PROP_0,

  PROP_WAITING_FOR_DATA,
};

static void
get_property (GObject    *object,
              guint       property_id,
              GValue     *value,
              GParamSpec *pspec)
{
  g_warning ("Unimplemented method");
}

static void
set_property (GObject      *object,
              guint         property_id,
              const GValue *value,
              GParamSpec   *pspec)
{
  g_warning ("Unimplemented method");
}

static void
lightwood_model_class_init (LightwoodModelClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GParamSpec *pspec;

  object_class->get_property = get_property;
  object_class->set_property = set_property;

  /**
   * ThornburyModel:waiting-for-data:
   *
   * When this property becomes TRUE, the model is stalled waiting for data.
   */
  pspec = g_param_spec_boolean ("waiting-for-data",
                                "Waiting for data",
                                "If TRUE, the model is waiting for data",
                                FALSE,
                                G_PARAM_READABLE);
  g_object_class_install_property (object_class, PROP_WAITING_FOR_DATA, pspec);
}

static void
lightwood_model_init (LightwoodModel *self)
{
}

