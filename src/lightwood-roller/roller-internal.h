/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __ROLLER_H__
#define __ROLLER_H__

#define __ROLLER_H_INSIDE__

#include <stdlib.h>

#include "liblightwood-model.h"
#include "liblightwood-roller.h"
#include "liblightwood-fixedroller.h"
#include "liblightwood-varroller.h"
#include "liblightwood-expandable.h"
#include "liblightwood-expander.h"
#include "liblightwood-cylinderdeform.h"
#include "liblightwood-deformturn.h"

#undef __ROLLER_H_INSIDE__

#endif /* __ROLLER_H__ */
