/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* multi_line.h
 *
 * Author: Dheeraj Kotagiri
 *
 * multi_line.h */

#ifndef _HAVE_LIGHTWOOD_MULTILINE_ENTRY_H
#define _HAVE_LIGHTWOOD_MULTILINE_ENTRY_H
#include <clutter/clutter.h>
#include <pango/pango.h>

G_BEGIN_DECLS

#define LIGHTWOOD_TYPE_MULTILINE_ENTRY lightwood_multiline_entry_get_type ()
G_DECLARE_DERIVABLE_TYPE (LightwoodMultilineEntry, lightwood_multiline_entry, LIGHTWOOD, MULTILINE_ENTRY, ClutterActor)

/**
 * LightwoodMultilineEntryClass:
 * @text_changed: class handler for the #LightwoodMultilineEntry::text_changed signal
 * @cursor_event: class handler for the #LightwoodMultilineEntry::cursor_event signal
 * @activate    : class handler for the #LightwoodMultilineEntry::activate signal
 *
 * The #LightwoodMultilineEntryClass struct contains only private data.
 *
 */
struct _LightwoodMultilineEntryClass
{
  ClutterActorClass parent_class;

  void (* line_count_changed)     (LightwoodMultilineEntry *self, gint linecount);
};

/* function to create mutli-line entry */
ClutterActor *lightwood_multiline_entry_new (void);

/* function to clear text from textbox */
void lightwood_multiline_entry_clear_text( LightwoodMultilineEntry *entry);

/* function to get text from  textbox */
const gchar *lightwood_multiline_entry_get_text(LightwoodMultilineEntry *entry);

/* function to set text font */
void lightwood_multiline_entry_set_font_name (LightwoodMultilineEntry *entry, const gchar  *font_name);

/* function to set color of text */
void lightwood_multiline_entry_set_color(LightwoodMultilineEntry *entry, const ClutterColor *color);

/* function to set text via keyEvent */
void lightwood_multiline_entry_handle_key_event(LightwoodMultilineEntry *entry,ClutterKeyEvent *key);

/* function to set properties for entry */
void lightwood_multiline_entry_set_properties (LightwoodMultilineEntry *entry,gint NLinesToDisplay, gboolean bEditable );

/* function to set width for textbox*/
void lightwood_multiline_entry_set_width(LightwoodMultilineEntry *entry, gint Width);

/* function to show next lines in text box */
gboolean lightwood_multiline_show_next_lines( LightwoodMultilineEntry *entry );

/* function to show previous lines in text box */
gboolean lightwood_multiline_show_prev_lines( LightwoodMultilineEntry *entry );

/* function to index line to particular x*/
void lightwood_multiline_index_to_line_x( LightwoodMultilineEntry *entry, guint char_pos, gint* line, gint* x_pos );

/* function to set cursor to focus*/
void lightwood_multiline_entry_set_cursor_visible(LightwoodMultilineEntry *entry, gboolean bVisible );

/* function to make entry to focus */
void lightwood_multiline_entry_grab_key_focus(LightwoodMultilineEntry *entry);

/* function to set height fortext box */
void lightwood_multiline_entry_set_height(LightwoodMultilineEntry *entry, gint height);

/* function to del a char from entry  */
void lightwood_multiline_entry_delete_char (LightwoodMultilineEntry *entry );

/* function to get default entry */
void lightwood_multiline_default_entry (LightwoodMultilineEntry *entry,
                                        gint keyval,
                                        const gchar *text);

ClutterActor *lightwood_multiline_get_active_text_actor (LightwoodMultilineEntry *entry);

void lightwood_multiline_display_entry_delete_char (LightwoodMultilineEntry *entry);

void lightwood_multiline_entry_delete_selected_text (LightwoodMultilineEntry *entry,
                                                    guint strlength);

void lightwood_multiline_entry_delete_normal_text (LightwoodMultilineEntry *entry,
                                                   guint strlength);

G_END_DECLS

#endif /* _HAVE_LIGHTWOOD_MULTILINE_ENTRY_H */
