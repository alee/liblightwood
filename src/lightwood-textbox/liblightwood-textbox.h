/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* liblightwood-textbox.h
 *
 * Author: Dheeraj Kotagiri
 *
 * text_box.h */

#ifndef __TEXT_BOX_H__
#define __TEXT_BOX_H__

#include <glib-object.h>
#include <clutter/clutter.h>
#include <string.h>
#include "liblightwood-multiline.h"

G_BEGIN_DECLS

#define LIGHTWOOD_TYPE_TEXT_BOX lightwood_text_box_get_type ()
G_DECLARE_DERIVABLE_TYPE (LightwoodTextBox, lightwood_text_box, LIGHTWOOD, TEXT_BOX, ClutterActor)

/**
 * LightwoodTextBoxClass:
 *
 * The #LightwoodTextBoxClass struct contains only private data.
 *
 */
struct _LightwoodTextBoxClass
{
  ClutterActorClass parent_class;
};

void lightwood_text_box_clear_text (LightwoodTextBox *self);
void lightwood_text_box_shift_up (LightwoodTextBox *self);
void lightwood_text_box_shift_down (LightwoodTextBox *self);
void lightwood_text_box_select_text (LightwoodTextBox *self);

/* Properties */

gboolean lightwood_text_box_get_multi_line (LightwoodTextBox *self);
void lightwood_text_box_set_multi_line (LightwoodTextBox *self,
                                        gboolean multi_line);

gboolean lightwood_text_box_get_focus_cursor (LightwoodTextBox *self);
void lightwood_text_box_set_focus_cursor (LightwoodTextBox *self,
                                          gboolean focus);

gboolean lightwood_text_box_get_password_enable (LightwoodTextBox *self);
void lightwood_text_box_set_password_enable (LightwoodTextBox *self,
                                             gboolean enable);

gint lightwood_text_box_get_max_lines (LightwoodTextBox *self);
void lightwood_text_box_set_max_lines (LightwoodTextBox *self,
                                       gint max_lines);

guint lightwood_text_box_get_displayed_lines (LightwoodTextBox *self);
void lightwood_text_box_set_displayed_lines (LightwoodTextBox *self,
                                             guint displayed_lines);

gboolean lightwood_text_box_get_text_editable (LightwoodTextBox *self);
void lightwood_text_box_set_text_editable (LightwoodTextBox *self,
                                           gboolean editable);

const gchar * lightwood_text_box_get_font_type (LightwoodTextBox *self);
void lightwood_text_box_set_font_type (LightwoodTextBox *self,
                                       const gchar *font_type);

const gchar * lightwood_text_box_get_text_color (LightwoodTextBox *self);
void lightwood_text_box_set_text_color (LightwoodTextBox *self,
                                       const gchar *text_color);

const gchar * lightwood_text_box_get_background_color (LightwoodTextBox *self);
void lightwood_text_box_set_background_color (LightwoodTextBox *self,
                                              const gchar *background_color);

const gchar * lightwood_text_box_get_text (LightwoodTextBox *self);
void lightwood_text_box_set_text (LightwoodTextBox *self,
                                  const gchar *text);

gfloat lightwood_text_box_get_text_x (LightwoodTextBox *self);
void lightwood_text_box_set_text_x (LightwoodTextBox *self,
                                    gfloat xpos);

gfloat lightwood_text_box_get_text_y (LightwoodTextBox *self);
void lightwood_text_box_set_text_y (LightwoodTextBox *self,
                                    gfloat ypos);

G_END_DECLS

#endif /* __LIGHTWOOD_TEXT_BOX_H__ */
