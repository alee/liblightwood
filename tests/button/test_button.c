/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* test_button.c */

#include "test_button.h"

G_DEFINE_TYPE (TestButton, test_button, LIGHTWOOD_TYPE_BUTTON)

#define BUTTON_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), TEST_TYPE_BUTTON, TestButtonPrivate))

#define TEST_BUTTON_DEGUG(...)		g_print(__VA_ARGS__);	

static void test_button_pressed(GObject *pObject, gpointer pUserData);
static void test_button_released(GObject *pObject, gpointer pUserData);
//static void test_button_leave(ClutterActor *pButton, ClutterCrossingEvent *event, gpointer pUserData);
static void test_button_swiped(GObject *pObject, LightwoodSwipeDirection inDirection, gpointer pUserData);
static void test_long_press(GObject *pObject, gpointer pUserData);


struct _TestButtonPrivate
{
	ClutterActor *pRect;
	
};

# if 0
static void test_button_leave(ClutterActor *pButton,  ClutterCrossingEvent *event, gpointer pUserData)
{
        TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: %s\n", __FUNCTION__);
	test_button_released(pButton, NULL);
	TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: %s\n", clutter_actor_get_name(pButton));
}
# endif

static void test_long_press(GObject *pObject, gpointer pUserData)
{
        TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: %s\n", __FUNCTION__);
}

static void test_button_swiped(GObject *pObject, LightwoodSwipeDirection inDirection, gpointer pUserData)
{
	TestButton *pButton = TEST_BUTTON(pObject);
        TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: %s\n", __FUNCTION__);

	TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: %s\n", clutter_actor_get_name(CLUTTER_ACTOR(pButton)));
	switch(inDirection)
	{
		case LIGHTWOOD_SWIPE_DIRECTION_LEFT:
			TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: direction = LIGHTWOOD_SWIPE_DIRECTION_LEFT:\n");
			break;
		case LIGHTWOOD_SWIPE_DIRECTION_RIGHT:
			TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: direction = LIGHTWOOD_SWIPE_DIRECTION_RIGHT:\n");
			break;
		case LIGHTWOOD_SWIPE_DIRECTION_UP:
			TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: direction = LIGHTWOOD_SWIPE_DIRECTION_UP:\n");
			break;
		case LIGHTWOOD_SWIPE_DIRECTION_DOWN:
			TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: direction = LIGHTWOOD_SWIPE_DIRECTION_DOWN:\n");
			break;	
		case LIGHTWOOD_SWIPE_DIRECTION_UNKNOWN:
		default:
			TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: direction = UNKNOWN\n");	
	}
	test_button_released(pObject, NULL);	
}

static void test_button_pressed(GObject *pObject, gpointer pUserData)
{
	TestButton *pButton = TEST_BUTTON(pObject);
	TestButtonPrivate *priv = BUTTON_PRIVATE (pButton);
	ClutterColor color = {0x00, 0xff, 0xff, 0xaa};

	TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: %s\n", __FUNCTION__);

	if(NULL != priv->pRect)
	{
		g_object_set(priv->pRect, "background-color", &color, NULL);
	}
	TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: %s\n", clutter_actor_get_name(CLUTTER_ACTOR(pButton)));
} 

static void test_button_released(GObject *pObject, gpointer pUserData)
{
	TestButton *pButton = TEST_BUTTON(pObject);
	TestButtonPrivate *priv = BUTTON_PRIVATE (pButton);
	ClutterColor white = {0xff, 0xff, 0xff, 0xff};

	TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: %s\n", __FUNCTION__);

        if(NULL != priv->pRect)
        {
                g_object_set(priv->pRect, "background-color", &white, NULL);
        }

	TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: %s\n", clutter_actor_get_name(CLUTTER_ACTOR(pButton)));
} 

static void test_button_get_property (GObject    *object,
                          guint       property_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void test_button_set_property (GObject      *object,
                          guint         property_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void test_button_dispose (GObject *object)
{
  G_OBJECT_CLASS (test_button_parent_class)->dispose (object);
}

static void test_button_finalize (GObject *object)
{
  G_OBJECT_CLASS (test_button_parent_class)->finalize (object);
}

static void test_button_class_init (TestButtonClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (TestButtonPrivate));

  object_class->get_property = test_button_get_property;
  object_class->set_property = test_button_set_property;
  object_class->dispose = test_button_dispose;
  object_class->finalize = test_button_finalize;
}

static void test_button_init (TestButton *self)
{
	ClutterColor white = {0xff, 0xff, 0xff, 0xff};

	self->priv = BUTTON_PRIVATE (self);
	g_object_set(self, "width", 64.0, "height", 64.0, NULL);
		

	//self->priv->pRect = clutter_rectangle_new_with_color(&white);
	self->priv->pRect = clutter_actor_new();
	g_object_set(self->priv->pRect, "background-color", &white, NULL);
	clutter_actor_set_size(self->priv->pRect, 64, 64);
	
	clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pRect);

	/* Lightwood button signals */
	g_signal_connect(self, "button-press", G_CALLBACK(test_button_pressed), NULL);
        g_signal_connect(self, "button-release", G_CALLBACK(test_button_released), NULL);
        g_signal_connect(self, "swipe-action", G_CALLBACK(test_button_swiped), NULL);
        g_signal_connect(self, "button-long-press", G_CALLBACK(test_long_press), NULL);
        
	/* leave signal */
	//g_signal_connect(self, "leave-event", G_CALLBACK(test_button_leave), NULL);

}

ClutterActor *test_button_new (void)
{
  return g_object_new (TEST_TYPE_BUTTON, NULL);
}

int main (int argc, char **argv)
{
	int clInErr = clutter_init(&argc, &argv);
	gfloat width, height;
	gboolean bLongPress, bSwipe;
	gchar *name = NULL;
        ClutterActor *pStage;
        ClutterColor black = { 0x00, 0x00, 0x00, 0x00 };
	ClutterActor *pButton, *pButton2;

        if (clInErr != CLUTTER_INIT_SUCCESS)
                return -1;

	//clutter_threads_init ();

        pStage = clutter_stage_new ();
        clutter_actor_set_background_color (pStage, &black);
        clutter_actor_set_size (pStage, 728, 480);
	g_signal_connect (pStage, "destroy", G_CALLBACK (clutter_main_quit), NULL);

	pButton = test_button_new();
	g_object_set(pButton, "swipe-enabled", TRUE, "reactive", TRUE, "long-press-enabled", TRUE, "name", "Button1", NULL);

	clutter_actor_add_child(pStage, pButton);

	pButton2 = test_button_new();
        g_object_set(pButton2, "swipe-enabled", FALSE, "long-press-enabled", FALSE, "reactive", TRUE, "name", "Button2", NULL);
	clutter_actor_set_position(pButton2, 65, 0);

	clutter_actor_add_child(pStage, pButton2);

	g_object_get(pButton2, "name", &name, "width", &width, "height", &height, "long-press-enabled", &bLongPress, "swipe-enabled", &bSwipe, NULL);
	TEST_BUTTON_DEGUG("Button2 :\n name = %s width = %f height = %f long-press-enabled = %d swipe-enabled = %d\n", name, width, height, bLongPress, bSwipe);

	g_object_get(pButton, "name", &name, "width", &width, "height", &height, "long-press-enabled", &bLongPress, "swipe-enabled", &bSwipe, NULL);
	TEST_BUTTON_DEGUG("Button1 : \n name = %s width = %f height = %f long-press-enabled = %d swipe-enabled = %d\n", name, width, height, bLongPress, bSwipe);

        clutter_actor_show (pStage);

        clutter_main();

	return 0;
}	













