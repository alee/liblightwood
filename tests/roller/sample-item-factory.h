/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2011 Robert Bosch Car Multimedia GmbH
 * Author: Tomeu Vizoso <tomeu.vizoso@collabora.co.uk>
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _SAMPLE_ITEM_FACTORY_H
#define _SAMPLE_ITEM_FACTORY_H

#include <glib-object.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <mx/mx.h>
#pragma GCC diagnostic pop

G_BEGIN_DECLS

#define TYPE_SAMPLE_ITEM_FACTORY sample_item_factory_get_type()

#define SAMPLE_ITEM_FACTORY(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  TYPE_SAMPLE_ITEM_FACTORY, SampleItemFactory))

#define SAMPLE_ITEM_FACTORY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  TYPE_SAMPLE_ITEM_FACTORY, SampleItemFactoryClass))

#define IS_SAMPLE_ITEM_FACTORY(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  TYPE_SAMPLE_ITEM_FACTORY))

#define IS_SAMPLE_ITEM_FACTORY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  TYPE_SAMPLE_ITEM_FACTORY))

#define SAMPLE_ITEM_FACTORY_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  TYPE_SAMPLE_ITEM_FACTORY, SampleItemFactoryClass))

typedef struct _SampleItemFactory SampleItemFactory;
typedef struct _SampleItemFactoryClass SampleItemFactoryClass;
typedef struct _SampleItemFactoryPrivate SampleItemFactoryPrivate;

struct _SampleItemFactory
{
  GObject parent;

  SampleItemFactoryPrivate *priv;
};

struct _SampleItemFactoryClass
{
  GObjectClass parent_class;
};

GType sample_item_factory_get_type (void) G_GNUC_CONST;

SampleItemFactory *sample_item_factory_new (void);

G_END_DECLS

#endif /* _SAMPLE_ITEM_FACTORY_H */
