/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sample-ui-elements.h"

#include <math.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <mx/mx.h>
#pragma GCC diagnostic pop

ClutterActor *
create_status_bar (void)
{
  ClutterActor *status_bar;
  ClutterActor *texture;
  ClutterActor *label;
  ClutterColor white = {255, 255, 255, 255};
  g_autofree gchar *status_bar_filename = NULL;
  g_autofree gchar *default_icon_filename = NULL;

  status_bar = clutter_group_new ();

  status_bar_filename = g_test_build_filename (G_TEST_DIST, "data",
                                               STATUS_BAR_FILE_NAME, NULL);
  texture = clutter_texture_new_from_file (status_bar_filename, NULL);
  clutter_actor_set_position (texture, 0, 0);
  clutter_container_add_actor (CLUTTER_CONTAINER (status_bar), texture);
  clutter_actor_set_reactive (texture, TRUE);
  clutter_actor_show (texture);

  default_icon_filename = g_test_build_filename (G_TEST_DIST, "data",
                                                 DEFAULT_ICON_FILE_NAME, NULL);
  texture = clutter_texture_new_from_file (default_icon_filename, NULL);
  clutter_actor_set_position (texture, 15, 7);
  clutter_container_add_actor (CLUTTER_CONTAINER (status_bar), texture);
  clutter_actor_show (texture);

  label = clutter_text_new_full ("Sans 24px", "MultiMedia Demonstrator", &white);
  clutter_container_add_actor (CLUTTER_CONTAINER (status_bar), label);
  clutter_actor_set_position (label, 76, 16);
  clutter_actor_set_width (label, 500);
  clutter_text_set_ellipsize (CLUTTER_TEXT (label), PANGO_ELLIPSIZE_END);
  clutter_actor_show (label);

  return status_bar;
}

ClutterModel *
create_model (guint num_of_items)
{
  CoglHandle icon1, icon2, thumb1, thumb2;
  CoglHandle covers[4];
  ClutterModel *model;
  int i;
  g_autofree gchar *icon1_filename = NULL, *icon2_filename = NULL;
  g_autofree gchar *cover1_filename = NULL, *cover2_filename = NULL;
  g_autofree gchar *cover3_filename = NULL, *cover4_filename = NULL;
  g_autofree gchar *thumb1_filename = NULL, *thumb2_filename = NULL;
  g_autofree gchar *video1_filename = NULL, *video2_filename = NULL;

  icon1_filename = g_test_build_filename (G_TEST_DIST, "data",
                                          ICON1_FILE_NAME, NULL);
  icon2_filename = g_test_build_filename (G_TEST_DIST, "data",
                                          ICON2_FILE_NAME, NULL);
  cover1_filename = g_test_build_filename (G_TEST_DIST, "data",
                                           COVER1_FILE_NAME, NULL);
  cover2_filename = g_test_build_filename (G_TEST_DIST, "data",
                                           COVER2_FILE_NAME, NULL);
  cover3_filename = g_test_build_filename (G_TEST_DIST, "data",
                                           COVER3_FILE_NAME, NULL);
  cover4_filename = g_test_build_filename (G_TEST_DIST, "data",
                                           COVER4_FILE_NAME, NULL);
  thumb1_filename = g_test_build_filename (G_TEST_DIST, "data",
                                           THUMB1_FILE_NAME, NULL);
  thumb2_filename = g_test_build_filename (G_TEST_DIST, "data",
                                           THUMB2_FILE_NAME, NULL);
  video1_filename = g_test_build_filename (G_TEST_DIST, "data",
                                           VIDEO1_FILE_NAME, NULL);
  video2_filename = g_test_build_filename (G_TEST_DIST, "data",
                                           VIDEO2_FILE_NAME, NULL);

  icon1 = cogl_texture_new_from_file (icon1_filename, COGL_TEXTURE_NONE,
                                      COGL_PIXEL_FORMAT_ANY, NULL);

  icon2 = cogl_texture_new_from_file (icon2_filename, COGL_TEXTURE_NONE,
                                      COGL_PIXEL_FORMAT_ANY, NULL);


  covers[0] = cogl_texture_new_from_file (cover1_filename, COGL_TEXTURE_NONE,
                                          COGL_PIXEL_FORMAT_ANY, NULL);

  covers[1] = cogl_texture_new_from_file (cover2_filename, COGL_TEXTURE_NONE,
                                          COGL_PIXEL_FORMAT_ANY, NULL);

  covers[2] = cogl_texture_new_from_file (cover3_filename, COGL_TEXTURE_NONE,
                                          COGL_PIXEL_FORMAT_ANY, NULL);

  covers[3] = cogl_texture_new_from_file (cover4_filename, COGL_TEXTURE_NONE,
                                          COGL_PIXEL_FORMAT_ANY, NULL);


  thumb1 = cogl_texture_new_from_file (thumb1_filename, COGL_TEXTURE_NONE,
                                       COGL_PIXEL_FORMAT_ANY, NULL);

  thumb2 = cogl_texture_new_from_file (thumb2_filename, COGL_TEXTURE_NONE,
                                       COGL_PIXEL_FORMAT_ANY, NULL);

  model = clutter_list_model_new (COLUMN_LAST, G_TYPE_STRING, NULL,
                                               COGL_TYPE_HANDLE, NULL,
                                               G_TYPE_STRING, NULL,
                                               G_TYPE_BOOLEAN, NULL,
                                               G_TYPE_STRING, NULL,
                                               G_TYPE_FLOAT, NULL,
                                               COGL_TYPE_HANDLE, NULL,
                                               COGL_TYPE_HANDLE, NULL,
                                               G_TYPE_STRING, NULL,
                                               -1);

  for (i = 0; i < num_of_items; i++)
    clutter_model_append (model, COLUMN_NAME, g_strdup_printf ("item number %i", i),
                                 COLUMN_ICON, i % 2 == 0 ? icon1 : icon2,
                                 COLUMN_LABEL, g_strdup_printf ("item number %i", i),
                                 COLUMN_TOGGLE, i % 2,
                                 COLUMN_VIDEO, i % 2 == 0 ? video1_filename : video2_filename,
                                 COLUMN_EXTRA_HEIGHT, (float) (i % 100),
                                 COLUMN_COVER, covers[i % 3],
                                 COLUMN_THUMB, i % 2 == 0 ? thumb1 : thumb2,
                                 COLUMN_LONG_TEXT, g_strdup_printf (LONG_TEXT, i),
                                 -1);

  return model;
}

static gdouble
ease_out_elastic_smooth (ClutterAlpha *alpha,
                         gpointer      dummy G_GNUC_UNUSED)
{
  ClutterTimeline *timeline = clutter_alpha_get_timeline (alpha);
  gdouble t = clutter_timeline_get_elapsed_time (timeline);
  gdouble d = clutter_timeline_get_duration (timeline);
  gdouble p = d * .3;
  gdouble s = p / 4;
  gdouble q = t / d;
  gdouble angle;
  gdouble result;

  if (q == 1)
    return 1.0;

  /* Taken from clutter_ease_out_elastic */
  angle = (q * d - s) * (2 * G_PI) / p;

  /* Move function to the right */
  angle -= G_PI / 2;

  /* Move in a straight line until the target value */
  if (angle < 0.)
    {
      gfloat slope = 1 / 0.15;
      result = q * slope;
    }
  else
    result = pow (2, -10 * q) * sin (angle) + 1.0;

  return result;
}

gulong
get_elastic_mode (void)
{
  static gulong clamp_mode = 0;

  if (G_UNLIKELY (clamp_mode == 0))
    clamp_mode = clutter_alpha_register_func (ease_out_elastic_smooth, NULL);

  return clamp_mode;
}

ClutterActor *
create_scroll_view (void)
{
  return g_object_new (MX_TYPE_KINETIC_SCROLL_VIEW,
                       "acceleration-factor", 2.0,
                       "deceleration", 1.03,
                       "clamp-duration", 1250,
                       "clamp-to-center", TRUE,
                       "overshoot", 1.0,
                       NULL);
}
